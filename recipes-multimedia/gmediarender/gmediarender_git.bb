SUMMARY = "gmrender-resurrect: UPnP/DLNA renderer"
HOMEPAGE = "https://github.com/hzeller/gmrender-resurrect"
LICENSE = "GPLv2+"

LIC_FILES_CHKSUM = "file://README;md5=c0dc4d93e06646a9ef36353ddcd86751"

inherit autotools pkgconfig

DEPENDS = "libupnp gstreamer1.0"

SRC_URI = "git://github.com/hzeller/gmrender-resurrect.git;protocol=https;branch=master \
"

# latest / master HEAD
SRCREV = "61f5a8fe7291995fec73956c7425fdb564e3cb9f"
PV = "0.0.7+git${SRCPV}"

# for license file
SRC_URI[md5sum] = "152dd5824e42f9435f7edfd049f384d5"
SRC_URI[sha256sum] = "68eaa7da71e64c4aeeb097d5095e6f7f6f4554031b15324abc4b3dfc685f9039"

S = "${WORKDIR}/git"

