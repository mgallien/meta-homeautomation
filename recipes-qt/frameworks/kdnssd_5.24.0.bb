DESCRIPTION = "KDE Framework KDNSSD."

LICENSE = "LGPLv2.1"
LIC_FILES_CHKSUM = "file://COPYING.LIB;md5=2d5025d4aa3495befef8f17206a5b0a1"

DEPENDS = "extra-cmake-modules qtbase avahi"

PACKAGE_DEBUG_SPLIT_STYLE = "debug-without-src"

inherit cmake_qt5
require frameworks-common.inc

SRC_URI[md5sum] = "69aaa1205564792c18ddfdaac7d60d61"
SRC_URI[sha256sum] = "b5341091e70ede689dcae61f7315a269e2eec40ba98ab607eb5785011e286605"

FILES_${PN}-dev = " \
  ${libdir}/cmake/KF5DNSSD/KF5DNSSDTargets-relwithdebinfo.cmake \
  ${libdir}/cmake/KF5DNSSD/KF5DNSSDConfigVersion.cmake \
  ${libdir}/cmake/KF5DNSSD/KF5DNSSDConfig.cmake \
  ${libdir}/cmake/KF5DNSSD/KF5DNSSDTargets.cmake \
  ${libdir}/cmake/KF5DNSSD/KF5DNSSDTargets-noconfig.cmake \
  /usr/mkspecs/modules/qt_KDNSSD.pri \
  ${libdir}/libKF5DNSSD.so \
  ${includedir}/KF5/KDNSSD/dnssd/domainbrowser.h \
  ${includedir}/KF5/KDNSSD/dnssd/kdnssd_export.h \
  ${includedir}/KF5/KDNSSD/dnssd/servicebase.h \
  ${includedir}/KF5/KDNSSD/dnssd/domainmodel.h \
  ${includedir}/KF5/KDNSSD/dnssd/remoteservice.h \
  ${includedir}/KF5/KDNSSD/dnssd/servicemodel.h \
  ${includedir}/KF5/KDNSSD/dnssd/servicetypebrowser.h \
  ${includedir}/KF5/KDNSSD/dnssd/publicservice.h \
  ${includedir}/KF5/KDNSSD/dnssd/servicebrowser.h \
  ${includedir}/KF5/KDNSSD/DNSSD/ServiceTypeBrowser \
  ${includedir}/KF5/KDNSSD/DNSSD/RemoteService \
  ${includedir}/KF5/KDNSSD/DNSSD/ServiceBrowser \
  ${includedir}/KF5/KDNSSD/DNSSD/ServiceModel \
  ${includedir}/KF5/KDNSSD/DNSSD/PublicService \
  ${includedir}/KF5/KDNSSD/DNSSD/DomainModel \
  ${includedir}/KF5/KDNSSD/DNSSD/ServiceBase \
  ${includedir}/KF5/KDNSSD/DNSSD/DomainBrowser \
"

