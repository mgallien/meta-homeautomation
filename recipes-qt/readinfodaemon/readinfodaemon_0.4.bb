DESCRIPTION = "ReadInfo deamon"

LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://LICENSE;md5=e6a600fd5e1d9cbde2d983680233ad02"

PR = "r1"

DEPENDS = "extra-cmake-modules qtbase automatiq qtserialport"

inherit cmake_qt5 systemd

SYSTEMD_SERVICE_${PN} = "${BPN}.service"

FILESEXTRAPATHS_append := "${THISDIR}/${PN}:"

SRC_URI = " \
        https://gitlab.com/homeautomationqt/readinfodaemon/repository/archive.tar.gz?ref=${PV};downloadfilename=readinfodaemon-${PV}.tar.gz \
        file://${BPN}.service \
"

do_install_append() {
    #support for systemd
    install -d ${D}${systemd_unitdir}/system/
    install -m 0644 ${WORKDIR}/${BPN}.service ${D}${systemd_unitdir}/system/${BPN}.service
}

SRCREV = "4083d171f2df7144d1f4f45bff9f06879bdd6d64"

S = "${WORKDIR}/${BP}-${SRCREV}"

SRC_URI[md5sum] = "39b34f2a620231ac20ff5df9afa83f2e"
SRC_URI[sha256sum] = "489df8932f705cabd7561bbadc8eff20b231e5e9712f1004911b1612512db0a3"

