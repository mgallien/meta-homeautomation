DESCRIPTION = "Automatiq Framework"

LICENSE = "LGPLv3+"
LIC_FILES_CHKSUM = "file://LICENSE;md5=e6a600fd5e1d9cbde2d983680233ad02"

DEPENDS = "extra-cmake-modules qtbase"

PACKAGE_DEBUG_SPLIT_STYLE = "debug-without-src"

inherit cmake_qt5

SRC_URI = "https://gitlab.com/homeautomationqt/automatiq/repository/archive.tar.gz?ref=${PV};downloadfilename=automatiq-${PV}.tar.gz"

SRCREV = "12f9c40eac166dba26aadf3df0b336792c381eb5"

S = "${WORKDIR}/${BP}-${SRCREV}"

SRC_URI[md5sum] = "57d212a4309f5ffbbf35ff05d2b3a26e"
SRC_URI[sha256sum] = "eda46a36d2531a1246e1b0939c2544749863c39bb06b95484723b3955caf16b8"

FILES_${PN} += " \
  ${libdir}/plugins/libAutomatiqProtocol.so \
"

FILES_${PN}-dev = " \
  ${includedir}/automatiq_version.h \
  ${includedir}/KF5/automatiq_version.h \
  ${includedir}/Automatiq \
  ${includedir}/Automatiq/MessageContainer \
  ${includedir}/Automatiq/MessageHello \
  ${includedir}/Automatiq/MessageGetVariables \
  ${includedir}/Automatiq/MessageListVariables \
  ${includedir}/Automatiq/DataVariableIdentifier \
  ${includedir}/Automatiq/DataVariableWithValue \
  ${includedir}/Automatiq/MessageListVariableValues \
  ${includedir}/Automatiq/NetworkPeer \
  ${includedir}/Automatiq/MessageAcknowledge \
  ${includedir}/Automatiq/PeerManager \
  ${includedir}/Automatiq/PeerProtocol \
  ${includedir}/Automatiq/ProtocolManager \
  ${includedir}/Automatiq/MessageError \
  ${includedir}/Automatiq/ConfigProtocol \
  ${includedir}/Automatiq/MessageGetConfiguration \
  ${includedir}/Automatiq/MessageConfiguration \
  ${includedir}/Automatiq/ServerConfigManager \
  ${includedir}/Automatiq/ClientConfigManager \
  ${includedir}/Automatiq/DataManager \
  ${includedir}/Automatiq/AbstractProtocol \
  ${includedir}/Automatiq/FeatureManager \
  ${includedir}/Automatiq/UniqueIdentifier \
  ${includedir}/Automatiq/AutomatiqClient \
  ${includedir}/Automatiq/AutomatiqHost \
  ${includedir}/Automatiq/CommonDeclarations \
  ${includedir}/Automatiq/AutomatiqNode \
  ${includedir}/Automatiq/automatiq \
  ${includedir}/Automatiq/automatiq/automatiq_export.h \
  ${includedir}/Automatiq/automatiq/messagecontainer.h \
  ${includedir}/Automatiq/automatiq/messagehello.h \
  ${includedir}/Automatiq/automatiq/messagegetvariables.h \
  ${includedir}/Automatiq/automatiq/messagelistvariables.h \
  ${includedir}/Automatiq/automatiq/datavariableidentifier.h \
  ${includedir}/Automatiq/automatiq/datavariablewithvalue.h \
  ${includedir}/Automatiq/automatiq/messagelistvariablevalues.h \
  ${includedir}/Automatiq/automatiq/networkpeer.h \
  ${includedir}/Automatiq/automatiq/messageacknowledge.h \
  ${includedir}/Automatiq/automatiq/peermanager.h \
  ${includedir}/Automatiq/automatiq/peerprotocol.h \
  ${includedir}/Automatiq/automatiq/protocolmanager.h \
  ${includedir}/Automatiq/automatiq/messageerror.h \
  ${includedir}/Automatiq/automatiq/configprotocol.h \
  ${includedir}/Automatiq/automatiq/messagegetconfiguration.h \
  ${includedir}/Automatiq/automatiq/messageconfiguration.h \
  ${includedir}/Automatiq/automatiq/serverconfigmanager.h \
  ${includedir}/Automatiq/automatiq/clientconfigmanager.h \
  ${includedir}/Automatiq/automatiq/datamanager.h \
  ${includedir}/Automatiq/automatiq/abstractprotocol.h \
  ${includedir}/Automatiq/automatiq/featuremanager.h \
  ${includedir}/Automatiq/automatiq/uniqueidentifier.h \
  ${includedir}/Automatiq/automatiq/automatiqclient.h \
  ${includedir}/Automatiq/automatiq/automatiqhost.h \
  ${includedir}/Automatiq/automatiq/commondeclarations.h \
  ${includedir}/Automatiq/automatiq/automatiqnode.h \
  ${libdir}/libAutomatiqCore.so \
  ${libdir}/libAutomatiqExchanges.so \
  ${libdir}/libAutomatiqProtocol.so \
  ${libdir}/cmake \
  ${libdir}/cmake/Automatiq \
  ${libdir}/cmake/Automatiq/AutomatiqConfig.cmake \
  ${libdir}/cmake/Automatiq/AutomatiqConfigVersion.cmake \
  ${libdir}/cmake/Automatiq/AutomatiqTargets.cmake \
  ${libdir}/cmake/Automatiq/AutomatiqTargets-noconfig.cmake \
"

